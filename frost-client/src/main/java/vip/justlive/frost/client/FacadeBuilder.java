package vip.justlive.frost.client;

import lombok.experimental.UtilityClass;
import vip.justlive.frost.api.facade.JobApiFacade;
import vip.justlive.oxygen.core.config.ConfigFactory;

/**
 * 代理类
 *
 * @author wubo
 */
@UtilityClass
public class FacadeBuilder {

  /**
   * 创建代理
   *
   * @param location 配置文件路径
   * @return facade
   */
  public static JobApiFacade build(String... location) {
    ConfigFactory.loadProperties(location);
    ClientProperties clientProps = ConfigFactory.load(ClientProperties.class);
    return new JobApiFacadeImpl(clientProps);
  }

  /**
   * 创建代理
   *
   * @param properties 配置属性
   * @return facade
   */
  public static JobApiFacade build(ClientProperties properties) {
    return new JobApiFacadeImpl(properties);
  }

}
